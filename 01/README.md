### Question

Di Indonesia, ada pecahan mata uang rupiah, yaitu :

- 100.000,-
- 50.000,-
- 20.000,-
- 10.000,-
- 5.000,-
- 2.000,-
- 1.000,-
- 500,-
- 200,-
- 100,-

Buatlah sebuah fungsi untuk menghitung berapa lembar pecahan yang harus dikeluarkan dari input harga (dengan pembulatan ke atas jita punya harga pecahan antara 1 sampai 99)

### Input/Output Expectations
```
Input: 145.000
Output: {
  “Rp. 100.000”: 1,
  “Rp. 20.000”: 2,
  “Rp. 5.000”: 1,
}
```
```
Input: 2050
Output: {
  “Rp. 2.000”: 1,
  “Rp. 100”: 1,
}
```