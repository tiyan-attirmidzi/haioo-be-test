package main

import (
	"fmt"
)

type Money struct {
	Nominal int
	Amount  int
}

func main() {
	for {
		moneyConvProgram()
	}
}

func moneyConvProgram() {

	var input int

	monies := []Money{}
	moneyFractions := []int{100000, 50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100}

	fmt.Print("Input : ")

	_, err := fmt.Scan(&input)

	if err != nil {
		fmt.Println("Error:", err)
	}

	for _, moneyFraction := range moneyFractions {
		if input != 0 {
			if input%1e2 >= 1 && input%1e2 <= 99 {
				input = (input - input%1e2) + 100
			}

			mod := input % moneyFraction
			currentInput := input - mod

			if input < moneyFraction {
				continue
			}

			monies = append(monies, Money{Nominal: moneyFraction, Amount: currentInput / moneyFraction})

			input = mod
		}
	}

	for _, money := range monies {
		fmt.Printf("Rp. %d : %d \n", money.Nominal, money.Amount)
	}
}
