### Question

Apakah ada kesalahan dari script di bawah ini? Jika ada tolong jelaskan dimana letak kesalahannya dan bagaimana anda memperbaikinya. Jika tidak ada, tolong jelaskan untuk apa script di bawah ini.

```bash
FROM golang
ADD . /go/src/github.com/telkomdev/indihome/backend
WORKDIR /go/src/github.com/telkomdev/indihome
RUN go get github.com/tools/godep
RUN godep restore
RUN go install github.com/telkomdev/indihome
ENTRYPOINT /go/bin/indihome
LISTEN 80
```

### Answer

Letak kesalahan pada script Dockerfile diatas terdapat pada script:

```
WORKDIR /go/src/github.com/telkomdev/indihome
```
Seharusnya directory script di atas adalah `/go/src/github.com/telkomdev/indihome/backend`, mangapa? karena pada instruksi `ADD` sebelumnya kita menambahkan file/folder ke dalam `/go/src/github.com/telkomdev/indihome/backend`, bukan `/go/src/github.com/telkomdev/indihome`.

```
LISTEN 80
```
Docker tidak memiliki insturksi `LISTEN`.