package main

import (
	"fmt"
	"math"
)

func main() {
	for {
		var input1, input2 string

		fmt.Print("Input 1: ")
		_, err := fmt.Scan(&input1)

		if err != nil {
			fmt.Println("Error:", err)
		}

		fmt.Print("Input 2: ")
		_, err = fmt.Scan(&input2)

		if err != nil {
			fmt.Println("Error:", err)
		}

		result := isUpdatedOne(input1, input2)

		fmt.Println("Result =>", result)
	}
}

func isUpdatedOne(input1, input2 string) bool {
	var lenInput1 int = len(input1)
	var lenInput2 int = len(input2)

	i := 0
	j := 0
	counter := 0

	if int(math.Abs(float64(lenInput1)-float64(lenInput2))) > 1 {
		return false
	}

	for i < lenInput1 && j < lenInput2 {
		if input1[i] != input2[j] {
			if counter == 1 {
				return false
			}
			if lenInput1 > lenInput2 {
				i++
			} else if lenInput1 < lenInput2 {
				j++
			} else {
				i++
				j++
			}
			counter++
		} else {
			i++
			j++
		}
	}

	if i < lenInput1 || j < lenInput2 {
		counter++
	}

	return counter == 1
}
